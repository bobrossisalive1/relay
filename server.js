'use strict'

import http from 'http'
import Gun from 'gun'

const port = (process.env.PORT || 8080)
const host = (process.env.HOST || '0.0.0.0')
const localStorage = (process.env.LOCALSTORAGE || 'false')
const radisk = (process.env.RADISK || 'true')
const axe = (process.env.AXE || 'false')
const profile = (process.env.PROFILE || 'false')

// Serve a static landing page
const requestListener = function (req, res) {
    res.writeHead(200)
    res.end(`{"error":"This is not for you, but I see you!"}`)
}

// Create the webserver
const server = http.createServer(requestListener)
.listen(port, host, function () {
    console.log(`server listening on port: ${port}`)
})
const gun = Gun({
    web: server,
    file: './gun',
    localStorage: localStorage === 'true' ? true : false,
    radisk: radisk === 'true' ? true : false,
    axe: axe === 'true' ? true : false
})

// Function to convert bytes to megabytes
function bytesToMB(bytes) {
    return bytes / (1024 * 1024);
}

function profileMemory() {
    // Get memory usage statistics
    const memoryUsage = process.memoryUsage();
    const rssMB = bytesToMB(memoryUsage.rss);
    const heapTotalMB = bytesToMB(memoryUsage.heapTotal);
    const heapUsedMB = bytesToMB(memoryUsage.heapUsed);
    const externalMB = bytesToMB(memoryUsage.external);

    // Display memory usage in MB
    console.log(`server listening on port: ${port}`)
    console.log(`RSS Memory: ${rssMB.toFixed(2)} MB`);
    console.log(`Heap Total: ${heapTotalMB.toFixed(2)} MB`);
    console.log(`Heap Used: ${heapUsedMB.toFixed(2)} MB`);
    console.log(`External: ${externalMB.toFixed(2)} MB`);
    setTimeout(profileMemory, 1000)
}

if (profile === 'true') profileMemory()