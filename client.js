import Gun from 'gun'
import SEA from 'gun/sea.js'

const port = (process.env.PORT || 8080)
const radisk = (process.env.RADISK || 'true')
const axe = (process.env.AXE || 'false')

const gun = Gun({
    peers: [`ws://relay:${port}/gun`],
    file: '/gun/data',
    localStorage: false,
    radisk: radisk === 'true' ? true : false,
    axe: axe === 'true' ? true : false
})

export const randomString = (
    len,
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
) => {
    let text = ''
    for (let i = 0; i < len; i++) {
        text += chars.charAt(Math.floor(Math.random() * chars.length))
    }
    return text
}

async function queryGUN() {
    gun.get('src').get('brain').get(Math.random()).get(randomString(24)).get(Math.random()).get(randomString(24)).once(async (v) => {
        // console.log(Math.random())
    })
    setTimeout(queryGUN, 5)
}

queryGUN()